package starter.math;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import starter.steps.ExampleTestHelper;

//OLD one(without cucumber)@RunWith(SerenityRunner.class)
//@Narrative(text={"Weather is important."})
@RunWith(CucumberWithSerenity.class) // new one (with cucumber)
@CucumberOptions(
        features="src/test/resources/features",
        glue = "starter.steps",
        plugin = {"pretty", "html:target/cucumber-report/cucumber.html"})
public class ExampleTest {
    @Steps
    ExampleTestHelper exampleTestHelper;

    @Test
    public void verifyThatIstanbulIsInTurkeyUsingTheWeatherAPI() {
        exampleTestHelper.tryTofindWeatherOfCountry("Istanbul");
        exampleTestHelper.weatherSearchIsExecutedSuccesfully();
        exampleTestHelper.weatherResponseBelongsToCountry("Turkey");
    }

    @Test
    public void verifyThatIstanbulIsInCorrectLatLongUsingTheWeatherAPI(){
        exampleTestHelper.tryTofindWeatherOfCountry("Istanbul");
        exampleTestHelper.weatherSearchIsExecutedSuccesfully();
        exampleTestHelper.isCountryInBetweenLatLong("41.019", "28.965");
    }


    @Test
    public void verifyThatResponseCityNameIsEqualToIstanbul(){
        exampleTestHelper.tryTofindWeatherOfCountry("Istanbul");
        exampleTestHelper.weatherSearchIsExecutedSuccesfully();
        exampleTestHelper.isCityNameEqualsToCity("Istanbul");
    }
}
