package starter.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import static org.hamcrest.Matchers.is;
import io.cucumber.java.en.When;

public class ExampleTestStepDefinitions {
    private String WEATHER_API_BASE_URL = "http://api.weatherstack.com/current?access_key=cf75fcd39303424050c0431c17a7a7cf&query=";
    private Response response;

    @Given("We want to check weather on {string}")
    public void chechkWeather(String country){
        response = SerenityRest.when().get(WEATHER_API_BASE_URL + country);
    }

    @When("API returns result successfully")
    public void whenResponseSuccess(){
        response.then().statusCode(200);
    }

    @Then("We should see country is equal to {string}")
    public void isItEqualToGivenCountry(String country){
        response.then().body("location.country", is(country));
    }

    @Then("We should see city name is equal to {string}")
    public void isItEqualToGivenCity(String city){
        response.then().body("location.name", is(city));
    }

    @Then("We should see city located in latitude {string} and longitude {string}")
    public void isCityInLatLon(String lat, String lon){
        response.then().body("location.lat", is(lat)).body("location.lon", is(lon));
    }
}
