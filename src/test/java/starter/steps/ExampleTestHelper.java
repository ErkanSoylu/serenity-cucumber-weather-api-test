package starter.steps;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import static org.hamcrest.Matchers.is;

public class ExampleTestHelper {
    private String WEATHER_API_BASE_URL = "http://api.weatherstack.com/current?access_key=cf75fcd39303424050c0431c17a7a7cf&query=";
    private Response response;

    // NOTE the following steps is for classic serenity testing(serenity without cucumber)
    @Step
    public void tryTofindWeatherOfCountry(String country){
        response = SerenityRest.when().get(WEATHER_API_BASE_URL + country);
    }

    @Step
    public void weatherSearchIsExecutedSuccesfully(){
        response.then().statusCode(200);
    }

    @Step
    public void weatherResponseBelongsToCountry(String country){
        response.then().body("location.country", is(country));
    }

    @Step
    public void isCountryInBetweenLatLong(String lat, String lon){
        response.then().body("location.lat", is(lat))
                .body("location.lon", is(lon));
    }

    @Step
    public void isCityNameEqualsToCity(String cityName){
        response.then().body("location.name", is(cityName));
    }
}
