Feature: Weather API Check

  Scenario: Weather API city,country test
    # Try to change Istanbul or turkey to provoke failure,
    # or try with other city/country pairs such as Zurich/Switzerland to see success
    Given We want to check weather on "Istanbul"
    When API returns result successfully
    Then We should see country is equal to "Turkey"

  Scenario: Weather API city latitude longitude test
    # Try to change Istanbul or turkey to provoke failure,
    Given We want to check weather on "Istanbul"
    When API returns result successfully
    Then We should see city located in latitude "41.019" and longitude "28.965"

  Scenario: Weather API city name test
    # Try to change Istanbul or turkey to provoke failure,
    # or try with other city/country pairs such as Zurich/Switzerland to see success
    Given We want to check weather on "Istanbul"
    When API returns result successfully
    Then We should see city name is equal to "Istanbul"

    ############# FAILURE EXAMPLES ##########

#Scenario: FAIL, Weather API city,country test
   ###########  Try to change Istanbul or turkey to provoke failure,
  ###########  or try with other city/country pairs such as Zurich/Switzerland to see success
  ########### Given We want to check weather on "Istanbul"
   ########### When API returns result successfully
   ########### Then We should see country is equal to "Gana"

 ########### Scenario: FAIL, Weather API city latitude longitude test
    # Try to change Istanbul or turkey to provoke failure,
  ###########  Given We want to check weather on "Istanbul"
   ########### When API returns result successfully
  ###########  Then We should see city located in latitude "10.016" and longitude "28.965"

 ###########Scenario: FAIL, Weather API city name test
    # Try to change Istanbul or turkey to provoke failure,
    # or try with other city/country pairs such as Zurich/Switzerland to see success
  ###########  Given We want to check weather on "Istanbul"
  ###########  When API returns result successfully
   ###########Then We should see city name is equal to "Ankara"
