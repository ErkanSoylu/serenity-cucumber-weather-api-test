**Serenity BDD Framework Overview**
Serenity is an open-source reporting library that enables developers to write easily-understandable and better-structured acceptance criteria for test automation projects. In addition to generating meaningful reports for each test, the tool also shows the list of features tested in each test case. With the details of the executed tests and the requirements that were tested, Serenity BDD Framework allows you to better understand how an application works and what it does.

**What is BDD?**
BDD stands for Behavior-driven Development. It has actually evolved from Test-driven Development (TDD) that talks about writing the test cases before writing the code. BDD also takes the same approach but focuses on system behavior and user behavior. In a BDD environment, tests are written in shared language so that all stakeholders (technical and non-technical) will be able to understand how an application works. It also facilitates nice collaboration between different teams. The ability to write test cases in a common language such as English gives a big advantage for developers while preparing acceptance testing criteria. You can write test cases depicting real-time scenarios.

In a BDD approach, requirements are converted into user stories and valid user scenarios.

**An example of a BDD template is**

Given
When
Then
Another example is

As a (premium customer)
I want (to browse the new arrivals in the fashion accessories segment)
So that (I can order the best apparels)
BDD comes with a minimal learning curve owing to the usage of a shared language. It provides seamless collaboration between different teams. Here, you are defining the behavior and not the test. So, the acceptance criteria is defined before developing the code which is beneficial for business owners as they get a clear understanding of how the application behaves instead of thinking about the features.

Cucumber and Serenity are a couple of popular BDD frameworks.

Getting Started with Serenity BDD Framework
These components run Serenity BDD Framework. 

**They are all required.**

Java
IntelliJ IDE
Gradle
Serenity BDD Plugin


**- Step 1: Install Java**
To download Java, visit the following link:

https://www.oracle.com/in/java/technologies/javase-downloads.html

Download the Java Development Kit (JDK)


**- Step 2: Configure Environment Variables**
To use Java, you should configure environment variables. Add Java path under system variables and user variable settings. To do so,

Click on System -> Advanced System Settings -> Advanced -> Environment Variables
Check if Java path is added under system variables and user variables path.

**-Step 3: Install IDE**
 
To write and execute tests using Serenity BDD Framework, you need a modern IDE. You can either choose IntelliJ or Eclipse. To install IntelliJ, visit the following website.

https://www.jetbrains.com/idea/


**-Step 4: Install Gradle**

I create my own project as a **Maven Project**.

In order to execute tests and generate reports, you should add a built tool such as Maven or Gradle. You can download Maven here:

https://maven.apache.org/download.cgi

For Gradle, visit this link:

https://gradle.org/next-steps/?version=6.5.1&format=all

Create a folder called Gradle in C drive and unzip the Gradle package into that folder.

**-Step 5: Configure Environment Variable**
To use Gradle, you should configure environment variables. Follow the steps mentioned in Step 2 to open the Environment Variables setting. Add the following path under system variables.

C:\Gradle\gradle-6.5.1\bin
To check if Gradle is successfully configured, open a command prompt window and type the version command.

Gradle -version


Serenity Framework's Documentation Repository : https://serenity-bdd.github.io/theserenitybook/latest/index.html

Serenity Framework's Github Repository : https://github.com/serenity-bdd

I use this repository for API testing with Cucumber 6 
https://github.com/serenity-bdd/serenity-rest-starter

I choose Weather API from Any API - https://weatherstack.com/?utm_source=any-api&utm_medium=OwnedSites

Write a 3 test case with positively and negatively but add a comment line for negatives because maven gives me an error and cant run them properly.

1. is City and Country test.
2. is for city's latitude and longitude controls.
3. is check city's name is correct for given.

**You can find the report file under ..**/target/site/serenity/index.html**

Thanks for read.



